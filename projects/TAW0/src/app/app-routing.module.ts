import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewStoryComponent } from './new-story/new-story.component';
import { BestStoryComponent } from './best-story/best-story.component';
import { TopStoryComponent } from './top-story/top-story.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';


const routes: Routes = [
  {
    path: 'story/:id',
    component: StoryDetailComponent
  },
  {
    path: 'new',
    component: NewStoryComponent
  },
  {
    path: 'best',
    component: BestStoryComponent
  },
  {
    path: '',
    component: TopStoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
