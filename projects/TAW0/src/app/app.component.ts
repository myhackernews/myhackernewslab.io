import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'taw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'My Hacker News';
  constructor(
    private titleService: Title
  ) { }
  ngOnInit() {
    this.titleService.setTitle(this.title);
  }
}
