import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { NewStoryComponent } from './new-story/new-story.component';
import { HackerNewsStoryFormComponent } from './hacker-news-story-form/hacker-news-story-form.component';
import { BestStoryComponent } from './best-story/best-story.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TopStoryComponent } from './top-story/top-story.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';
import { LoadingComponent } from './loading/loading.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent,
    NewStoryComponent,
    HackerNewsStoryFormComponent,
    BestStoryComponent,
    TopStoryComponent,
    StoryDetailComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    StoreModule.forRoot({}, {})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
