import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestStoryComponent } from './best-story.component';

describe('BestStoryComponent', () => {
  let component: BestStoryComponent;
  let fixture: ComponentFixture<BestStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestStoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
