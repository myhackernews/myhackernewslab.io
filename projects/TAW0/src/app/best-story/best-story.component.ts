import { Component, OnInit } from '@angular/core';
import { HackerNewsService } from '../hacker-news.service';
import { Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'taw-best-story',
  templateUrl: './best-story.component.html',
  styleUrls: ['./best-story.component.sass']
})
export class BestStoryComponent implements OnInit {
  stories$: any;
  constructor(
    private hackerNewsService: HackerNewsService,
    private titleService: Title,
  ) {}
  ngOnInit(): void {
    this.setTitle();
    this.getBestStories();
  }
  getBestStories(): void {
    this.stories$ = this.hackerNewsService.getBestStories();
  }
  setTitle(): void {
    this.titleService.setTitle(`Top Stories | My Hacker News`);
  }
}
