import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HackerNewsStoryFormComponent } from './hacker-news-story-form.component';

describe('HackerNewsStoryFormComponent', () => {
  let component: HackerNewsStoryFormComponent;
  let fixture: ComponentFixture<HackerNewsStoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HackerNewsStoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HackerNewsStoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
