import { Component, OnInit, Input, ViewChild, NgZone } from '@angular/core';
import { HackerNewsService } from '../hacker-news.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StoryDetail } from '../story-detail';
import { LoadingService } from '../loading.service';
import { finalize, map, take } from 'rxjs/operators';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { UnprocessedStoryDetail } from '../unprocessed-story-detail';

@Component({
  selector: 'taw-hacker-news-story-form',
  templateUrl: './hacker-news-story-form.component.html',
  styleUrls: ['./hacker-news-story-form.component.sass']
})
export class HackerNewsStoryFormComponent implements OnInit {
  @Input() id: number;
  storyDetails: StoryDetail;
  storyForm: FormGroup;
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  constructor(
    private hackerNewsService: HackerNewsService,
    private formBuilder: FormBuilder,
    private loadingService: LoadingService,
    private ngZone: NgZone
  ) { }
  ngOnInit(): void {
    this.storyForm = this.createStoryForm();
    this.getStoryDetails(this.id);
  }
  resetForm() {
    this.storyForm.reset();
    this.storyForm.patchValue({
      id: this.storyDetails?.id,
      by: this.storyDetails?.by,
      title: this.storyDetails?.title,
      url: this.storyDetails?.url,
      type: this.storyDetails?.type,
      dateSubmitted: this.storyDetails?.dateSubmitted
    });
  }
  validateForm() {
    this.storyForm.disable();
  }
  createStoryForm(): FormGroup {
    const myForm = this.formBuilder.group({
      id: [''],
      by: [''],
      title: [''],
      type: [''],
      url: [''],
      dateSubmitted: ['']
    });
    return myForm;
  }
  getStoryDetails(id: number) {
    const storyDetail$ = this.hackerNewsService.getStoryDetails(id);
    this.loadingService.showLoaderUntilCompleted(storyDetail$).subscribe((data: UnprocessedStoryDetail) => {
      if (data && data.type === 'story') {
        this.storyDetails = new StoryDetail(data.id, data.by, data.title, data.url, data.type, data.score, data.time);
        this.validateForm();
        this.resetForm();
      }
    });
  }
  goToStory(url: string) {
    window.open(url, `_blank`);
  }
  goToComments(id: number) {
    window.open(`https://news.ycombinator.com/item?id=${id}`, `_blank`);
  }
  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }
}
