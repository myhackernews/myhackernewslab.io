import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, pipe, of } from 'rxjs';
import { catchError, retry, shareReplay, map } from 'rxjs/operators';
import { StoryDetail } from './story-detail';
import { UnprocessedStoryDetail } from './unprocessed-story-detail';
@Injectable({
  providedIn: 'root'
})
export class HackerNewsService {
  constructor(
    private http: HttpClient,
  ) { }
  createDummyStory(): StoryDetail {
    const returnModel = new StoryDetail(
      13722352,
      'antirez',
      'Redis on the Raspberry Pi: adventures in unaligned lands',
      'http://antirez.com/news/111',
      'story',
      7,
      1487929997);
    return returnModel;
  }
  getNewestStories() {
    // https://hacker-news.firebaseio.com/v0/newstories.json
    return this.http.get('https://hacker-news.firebaseio.com/v0/newstories.json');
  }
  getStoryDetails(storyId: number): Observable<UnprocessedStoryDetail> {
    // https://hacker-news.firebaseio.com/v0/item/8863.json
    const returnModel = this.http.get(`https://hacker-news.firebaseio.com/v0/item/${storyId}.json`) as Observable<UnprocessedStoryDetail>;
    return returnModel.pipe(shareReplay(1));
  }
  getBestStories(): Observable<any> {
    // https://hacker-news.firebaseio.com/v0/beststories.json
    return this.http.get(`https://hacker-news.firebaseio.com/v0/beststories.json`);
  }
  getTopStories(): Observable<any> {
    return this.http.get(`https://hacker-news.firebaseio.com/v0/topstories.json`);
  }
}
