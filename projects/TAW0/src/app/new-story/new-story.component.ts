import { Component, OnInit, OnDestroy } from '@angular/core';
import { HackerNewsService } from '../hacker-news.service';
import { Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'taw-new-story',
  templateUrl: './new-story.component.html',
  styleUrls: ['./new-story.component.sass']
})
export class NewStoryComponent implements OnInit {
  stories$: any;
  constructor(
    private hackerNewsService: HackerNewsService,
    private titleService: Title,
  ) { }
  ngOnInit(): void {
    this.setTitle();
    this.stories$ = this.hackerNewsService.getNewestStories();
  }
  setTitle(): void {
    this.titleService.setTitle(`Top Stories | My Hacker News`);
  }
}
