export class StoryDetail {
    // id: this.storyDetails?.id,
    // by: this.storyDetails?.by,
    // title: this.storyDetails?.title,
    // url: this.storyDetails?.url,
    // type: this.storyDetails?.type,
    id: number;
    by: string;
    title: string;
    url: string;
    type: string;
    score: number;
    dateSubmitted: Date;
    constructor(id: number, by: string, title: string, url: string, type: string, score: number, dateSubmittedUnix: number) {
        this.id = id;
        this.by = by;
        this.title = title;
        this.url = url;
        this.type = type;
        this.score = score;
        this.dateSubmitted = new Date(dateSubmittedUnix * 1000);
    }
}
