import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'taw-story-detail',
  templateUrl: './story-detail.component.html',
  styleUrls: ['./story-detail.component.sass']
})
export class StoryDetailComponent implements OnInit {
  id: number;
  constructor(
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
  ) { }

  ngOnInit(): void {
    this.getId();
  }
  getId() {
    this.activatedRoute.params.subscribe((params) => {
      this.id = +params.id;
      this.setTitle(this.id.toLocaleString());
    });
  }
  setTitle(title: string): void {
    this.titleService.setTitle(`Story: ${this.id} | My Hacker News`);
  }
}
