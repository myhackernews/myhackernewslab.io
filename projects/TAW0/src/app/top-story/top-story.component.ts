import { Component, OnInit } from '@angular/core';
import { HackerNewsService } from '../hacker-news.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'taw-top-story',
  templateUrl: './top-story.component.html',
  styleUrls: ['./top-story.component.sass']
})
export class TopStoryComponent implements OnInit {
  stories$: any;
  constructor(
    private hackerNewsService: HackerNewsService,
    private titleService: Title,
  ) {}
  ngOnInit(): void {
    this.setTitle();
    this.getTopStories();
  }
  setTitle(): void {
    this.titleService.setTitle(`Top Stories | My Hacker News`);
  }
  getTopStories(): void {
    this.stories$ = this.hackerNewsService.getTopStories();
  }
}
